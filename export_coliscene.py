import bmesh
import bpy
import math
import mathutils
import struct


from .header import Header
from .keyableattribute import InterpolationMode, KeyableAttribute
from .node import TrackNode, Plane, CheckPoint, TrackSegment, TrackCurves
from .sceneobject import SceneObjectDynamic, TransformPRXS, TransformMatrix3x4, SceneObject, SceneObjectLOD

# Messages
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_DATA_HEX = '{0}: {1:#X}'

# Name
NAME_ROOT_OBJ = '[ROOT]'
NAME_ROAD = '[ROAD]'
NAME_SEC = '[SEC]'
NAME_TRANS = '[TRANS]'
NAME_CHECKPOINT = '[POINT]_{0:04}'
NAME_CHECKPOINT_START = '{0}_in'
NAME_CHECKPOINT_END = '{0}_out'
NAME_ANIM = '[ANIM]'
NAME_SCENE = '[SCENE]'
NAME_BIND = '[BIND]'
NAME_COLLIDER = '[COLI]'
NAME_NAMES = '[NAMES]'
NAME_SCENE_OBJ = '[OBJ]'
NAME_NULL = '~NULL~'
FMT_ID = '{0}_{1:02}'
STR_VER = 'V05' # Export Script Version

CHAR_SET = 'ascii'


# Get Projection Length
def get_projection_length(direction, position):
    px = direction[0] * position[0]
    py = direction[1] * position[1]
    pz = direction[2] * position[2]

    value = px + py + pz
    return -value

# Generate NameString List
def generate_namestring(bl_dynamic_obj):
    def pick_up_name(str, name_strs):
        name_str = str
        splited = name_str.rsplit('.', 1)
        bind_name = splited[0]
        if (']_' in bind_name):
            # start with '[XXX]_' string
            splited = bind_name.split('_', 1)
            bind_name = splited[1]
        if bind_name not in name_strs:
            name_strs.append(bind_name)
    
    name_strs = []
    # Get '[NAMES]' object
    # this Object only exsit at OfficialTrack (Not Exsit at CustomTrack)
    bl_names_pickup = [ child for child in bl_dynamic_obj.children if NAME_NAMES in child.name ]
    if (len(bl_names_pickup) > 0):
        # Exist '[NAMES]' object
        bl_names_obj = bl_names_pickup[0]
        for bl_name_obj in bl_names_obj.children:
            # Get name strings without '00n_'
            name_str = bl_name_obj.name
            splited = name_str.split('_', 1)
            name_str = splited[1]
            name_strs.append(name_str)

    # Get '[BIND]' object
    bl_bind_pickup = [ child for child in bl_dynamic_obj.children if NAME_BIND in child.name ]
    bl_bind_obj = bl_bind_pickup[0]
    # Get NameString from child of '[BIND]' object
    for scene_obj in bl_bind_obj.children:
        pick_up_name(scene_obj.name, name_strs)
        for child in scene_obj.children:
            # Get name strings without '.00n'
            pick_up_name(child.name, name_strs)

    # add Export Script Version
    name_strs.append(STR_VER)

    return name_strs

# Generate SceneObject
def generate_sceneobject(binds, names_adr_dic):
    scene_objs = []
    for bind in binds:
        scene_obj = SceneObject()

        # SceneObjectLOD
        lod = SceneObjectLOD ()
        bind_name = bind.name
        if (']_' in bind_name):
            # start with '[XXX]_' string
            splited = bind_name.split('_', 1)
            bind_name = splited[1]
        name_ptr = names_adr_dic[bind_name]
        lod.lod_name_ptr = name_ptr
        scene_obj.lods.append(lod)
        # SceneObjectLOD(more than 2nd)
        lods = [ child for child in bind.children if NAME_COLLIDER not in child.name]
        for child in lods:
            # SceneObjectLOD
            lod = SceneObjectLOD ()
            # Get name strings without '.00n'
            name_str = child.name
            splited = name_str.rsplit('.', 1)
            bind_name = splited[0]
            name_ptr = names_adr_dic[bind_name]
            lod.lod_name_ptr = name_ptr
            scene_obj.lods.append(lod)
        
            # TODO: effect mesh

        scene_objs.append(scene_obj)
        scene_obj.lod_count = len(scene_obj.lods)

    return scene_objs

# Generate SceneObjectDynamic
def generate_sceneobjectdynamic(objs, scene_obj_adr_dic):
    dynamic_objs = []
    for obj in objs:
        dynamic_obj = SceneObjectDynamic()

        # TODO: settup from each object's custom property
        dynamic_obj.unk0x00 = 0x3
        dynamic_obj.unk0x04 = 0xFFFFFFFF

        # SceneObject Pointer
        scene_obj_name = obj.children[0].name
        # Get name strings without '.00n'
        splited = scene_obj_name.rsplit('.', 1)
        scene_obj_name = splited[0]
        scene_obj_ptr = scene_obj_adr_dic[scene_obj_name]
        dynamic_obj.scene_object_ptr = scene_obj_ptr

        # TransformMatrix
        obj_mtx = TransformMatrix3x4()
        matrix_local = obj.matrix_local
        obj_mtx.mtx[0:3] = matrix_local[0:3]
        dynamic_obj.transform_mtx = obj_mtx
        prxs = TransformPRXS()
        prxs.position = obj.location
        prxs.decomposed_rotation = obj.rotation_euler
        prxs.scale = obj.scale
        dynamic_obj.transform_prxs = prxs

        dynamic_objs.append(dynamic_obj)

    return dynamic_objs

# Generate KeyableAttribute
def generate_keyableattribute(point, is_euler):
    anim_key = KeyableAttribute()
    co = point.co
    # Time
    anim_key.time = 0.0
    if co[0] >= 1.0:
        anim_key.time = co[0] / 60.0
    # Value
    value = co[1]
    # TODO set FREE, ALIGNED, AUTO, AUTO_CLAMPED
    ease_mode = InterpolationMode.unknown1
    if (point.handle_left_type == point.handle_right_type):
        if (point.handle_left_type == 'VECTOR'):
            ease_mode = InterpolationMode.Linear
    else:
        print('Handle Left and Right not same type. Set same Type. Left:{0} Right:{1}'.format(point.handle_left_type, point.handle_right_type))
    anim_key.ease_mode = ease_mode

    # Tangent In
    z_tangent_in = value - point.handle_left[1]
    # Tangent Out
    z_tangent_out = point.handle_right[1] - value
    if (is_euler == True):
        # Convert Euler to Degrees 
        # F-Curve's Acctual value is stored as "Euler".
        # thus needs 
        value = math.degrees(value)
        z_tangent_in = math.degrees(z_tangent_in)
        z_tangent_out = math.degrees(z_tangent_out)
    anim_key.value = value
    anim_key.z_tangent_in = z_tangent_in
    anim_key.z_tangent_out = z_tangent_out
    return anim_key

# Generate TrackSegment
def generate_tracksegment(sections):
    # Generate TrackCurves
    def generate_trackcurves(fcurves):
        # Convert TRS(Blender) to SRT(F-ZERO GX)
        trs2srt_convert = [6, 7, 8, 3, 4, 5, 0, 1, 2]
        #      Translate  Rotation   Scale
        # TRS: [0, 1, 2], [3, 4, 5], [6, 7, 8]
        # SRT: [6, 7, 8], [3, 4, 5], [0, 1, 2]

        track_curves = TrackCurves()
        for i, fcurve in enumerate(fcurves):
            if ( (len(fcurve.keyframe_points) < 2) and (fcurve.keyframe_points[0].co[1] == 0.0) ):
                continue
            is_euler = fcurve.data_path == 'rotation_euler'
            for point in fcurve.keyframe_points:
                anim_key= generate_keyableattribute(point, is_euler)
                idx = trs2srt_convert[i]
                track_curves.keys_list[idx].append(anim_key)
        return track_curves

    segments = []

    for section in sections:
        segment = TrackSegment()
        segment.local_position = [ section.location[0], \
                              section.location[1], \
                              section.location[2] ]
        rot = [ math.degrees(rad) for rad in section.rotation_euler ]
        segment.local_rotation = [ rot[0], rot[1], rot[2] ]
        segment.local_scale = [ section.scale[0], \
                              section.scale[1], \
                              section.scale[2] ]

        transs = [ child for child in section.children if NAME_TRANS in child.name ]
        trans = transs[0]

        # TrackCurves
        anims = [ child for child in trans.children if NAME_ANIM in child.name ]
        anim = anims[0]
        track_curves = TrackCurves()
        if (anim.animation_data != None):
            # KeyFrames exsit
            fcurves = anim.animation_data.action.fcurves
            track_curves = generate_trackcurves(fcurves)
        segment.track_curves = track_curves

        # TODO: impletent child SEC
        # childs = []
        # trans.children.append(childs)

        segment.unk_0x3c = 6.0 # rail_height_right
        segment.unk_0x40 = 6.0 # rail_height_left
        segment.hierarchy_depth = 0x2
        segment.has_children = 0xC #(Left and Right Wall)

        segments.append(segment)

    return segments

# Generate CheckPoint
# TODO: Set "connectToTrackIn/Out" value from Each Object
def generate_checkpoint(check_pt, next_check_pt, in_time, out_time, check_pt_offset):
    # Convert angle to direction
    # y_reverse True:Rotate 180deg at Y-axis, False: not rotate.
    def euler2direction(euler, y_reverse=False):
        rot = mathutils.Quaternion(euler)
        if (y_reverse):
            up = rot * mathutils.Vector(( 0.0, 1.0, 0.0 ))
            _rot = mathutils.Quaternion( up, math.radians(180.0) )
            rot = _rot * rot
        vec = rot * mathutils.Vector(( 0.0, 0.0, -1.0 ))
        dir = [ vec[0], vec[1], vec[2] ]
        return dir

    pt_pos = mathutils.Vector(( check_pt.location[0], \
                                check_pt.location[1], \
                                check_pt.location[2] ))
    pt_pos_next = mathutils.Vector(( next_check_pt.location[0], \
                                        next_check_pt.location[1], \
                                        next_check_pt.location[2] ))
    point = CheckPoint()
    point.curve_time_start = in_time / 60.0 if in_time > 0.0 else in_time
    point.curve_time_end = out_time / 60.0 if out_time > 0.0 else out_time
    
    plane_start = Plane()
    normal_start = euler2direction(check_pt.rotation_euler)
    plane_start.dotProduct = get_projection_length(normal_start, pt_pos)
    plane_start.normal = normal_start
    plane_start.origin = [ pt_pos[0], pt_pos[1], pt_pos[2] ]
    point.plane_start = plane_start

    plane_end = Plane()
    normal_end = euler2direction(next_check_pt.rotation_euler, True)
    plane_end.dotProduct = get_projection_length(normal_end, pt_pos_next)
    plane_end.normal = normal_end
    plane_end.origin = [ pt_pos_next[0], pt_pos_next[1], pt_pos_next[2] ]
    point.plane_end = plane_end

    point.distance_start = check_pt_offset
    point_distance = pt_pos_next - pt_pos
    mag = point_distance.magnitude
    point.distance_end = check_pt_offset + mag

    point.width = check_pt.scale[0] # Scale-X is Width

    point.connect_to_track_in = True #TODO: Set this value from Each Object
    point.connect_to_track_out = True #TODO: Set this value from Each Object

    return point

# Generate TrackNode
def generate_tracknode(sections, segments, is_point2point):
    def objname2time(str):
        splited = str.rsplit('_t')
        time = 0
        if (len(splited) > 0):
            time = int(splited[1])
        else:
            time = -1.0 # this is ERROR
        return time

    track_node = []
    check_pt_offset = 0.0
    for (section, segments) in zip (sections, segments):
        check_pts = [check_pt for check_pt in section.children if NAME_TRANS not in check_pt.name]
        loop_max = len(check_pts)
        print(MSG_INFO_DATA.format('check_pts len', loop_max))
        print(check_pts)
        for i, check_pt in enumerate(check_pts):
            print(MSG_INFO_DATA.format('loop', i))
            if (i >= loop_max-1):
                break
            node = TrackNode()
            node.segment_ptr = segments.adr

            next_check_pt = check_pts[i+1]

            in_time = objname2time(check_pt.name) # get checkpoint's time
            out_time = objname2time(next_check_pt.name) # get next of checkpoint's time

            point = generate_checkpoint(check_pt, next_check_pt, in_time, out_time, check_pt_offset)
            check_pt_offset = point.distance_end

            node.points.append(point)
            track_node.append(node)
    
    # if (is_point2point != True):
    #     # set last distance is 0.0 
    #     points = track_node[-1].points
    #     for point in points:
    #         point.track_distance_end = 0.0

    return track_node

def write_padd(file, size):
    # padding (aligned by "size")
    modded = (file.tell() % size)
    if (modded != 0):
        pad_len = size - modded
        padd_list = [0x00] * pad_len
        pad_buff = struct.pack('{0}B'.format(pad_len), *padd_list)
        file.write(pad_buff)

# Pack String
def pack_string(file, str):
    fmt = '>{}s'.format(len(str))
    buff = struct.pack(fmt, bytes(str.encode(CHAR_SET)))
    file.write(buff)

# Export COLI_COURSE
def save(filepath, is_ax, is_point2point):
    with open(filepath, 'wb') as file:        
        header = Header()
        # Skip Header 
        file.seek(header.unk0x24 + 0x04)
        write_padd(file, 0x100) # this for Develop TODO: Remove this

        bl_root_child = bpy.data.objects[NAME_ROOT_OBJ].children

        # SceneObjectDynamic
        # Get '[SCENE]' object
        bl_secne_pickup = [ child for child in bl_root_child if NAME_SCENE in child.name ]
        bl_dynamic_obj = bl_secne_pickup[0]

        # NameString
        name_strs = [] # export this list strings
        name_strs = generate_namestring(bl_dynamic_obj)
        print(name_strs) # this for Develop TODO: Remove this
        names_adr_dic = {} # Links 'string' and 'SceneObjectLOD'
        for name_str in name_strs:
            names_adr_dic[name_str] = file.tell()
            if (name_str in NAME_NULL):
                # object name is export as null
                name_str = ''
            if (len(name_str) > 0):
                pack_string(file, name_str)
            #Padding
            file.write(bytes(1))
            write_padd(file, 0x04)
        print(names_adr_dic) # this for Develop TODO: Remove this
        write_padd(file, 0x100) # this for Develop TODO: Remove this

        # Get '[BIND]' object
        bl_bind_pickup = [ child for child in bl_dynamic_obj.children if NAME_BIND in child.name ]
        bl_bind_obj = bl_bind_pickup[0]
        bl_binds = bl_bind_obj.children
        # SceneObject
        scene_objs = generate_sceneobject(bl_binds, names_adr_dic)

        # Write effectmesh
        # TODO: effectmesh

        lod_count = 0x00
        # Write SceneObjectLOD
        for scene_obj in scene_objs:
            for lod in scene_obj.lods:
                lod.pack(file)
                lod_count += 1
            scene_obj.lod_ptr = scene_obj.lods[0].adr
        print(MSG_INFO_DATA_HEX.format('data0x70_ptr', scene_objs[0].lods[0].adr))
        write_padd(file, 0x100) # this for Develop TODO: Remove this

        scene_obj_adr_dic = {} # Links '[COLI]_n' and Address of 'SceneObject'
        # SceneObject
        bind_names = [binds.name for binds in bl_binds]
        for (bind_name, scene_obj) in zip (bind_names, scene_objs):
            scene_obj.pack(file)
            scene_obj_adr_dic[bind_name] = scene_obj.adr
        # Update Header
        header.data0x68_ptr = scene_objs[0].adr
        header.data0x68_count = len(scene_objs)
        print(scene_obj_adr_dic) # this for Develop TODO: Remove this

        # Data0x70
        # Update Header
        header.data0x70_count = 0x00
        header.data0x70_ptr = file.tell()
        write_padd(file, 0x100) # this for Develop TODO: Remove this

        # Get '[OBJ]' objects
        bl_dynamic_objs = [ child for child in bl_dynamic_obj.children if NAME_SCENE_OBJ in child.name ]
        print(bl_dynamic_objs)
        dynamic_objs = []
        print(MSG_INFO_DATA.format('bl_dynamic_objs len', len(bl_dynamic_objs)))
        dynamic_objs = generate_sceneobjectdynamic(bl_dynamic_objs, scene_obj_adr_dic)
        print(MSG_INFO_DATA.format('sceneobject len', len(dynamic_objs)))
        # TransformMatrix3x4
        mtxs = []
        cnt = 0
        for dynamic_obj in dynamic_objs:
            # Write TransformMatrix3x4
            transform_mtx_ptr = 0x00
            transform_mtx = dynamic_obj.transform_mtx
            print(MSG_INFO_DATA.format('times', cnt))
            cnt += 1
            match_list = list(filter(lambda x: x == transform_mtx, mtxs))
            if (len(match_list) == 0):
                transform_mtx.pack(file)
                transform_mtx_ptr = transform_mtx.adr
                mtxs.append(transform_mtx)
            else :
                transform_mtx_ptr = mtxs[transform_mtx].adr
            dynamic_obj.transform_ptr = transform_mtx_ptr
        write_padd(file, 0x100) # this for Develop TODO: Remove this
        # Update Header
        header.sceneobject_ptr = file.tell()
        header.sceneobject_count = len(dynamic_objs)
        header.unk0x48 = len(dynamic_objs)
        for dynamic_obj in dynamic_objs:
            dynamic_obj.pack(file)
        write_padd(file, 0x100) # this for Develop TODO: Remove this

        # Get '[ROAD]' object
        bl_road_pickup = [ child for child in bl_root_child if NAME_ROAD in child.name ]
        bl_road_obj = bl_road_pickup[0]
        # Get '[SEC]_***' objects
        bl_sections = [ child for child in bl_road_obj.children if NAME_SEC in child.name ]
        total_distance = 0.0
        if (len(bl_sections) > 0):
            # TrackSegment
            segments = []
            segments = generate_tracksegment(bl_sections)
            for segment in segments:
                # Write TrackSegment (include TrackCurves and children of TrackSegment)
                segment.pack_root(file)
                offs = segment.adr

            write_padd(file, 0x100) # this for Develop TODO: Remove this

            for tran in segments:
                print(MSG_INFO_DATA_HEX.format('transform_offsets', tran.adr))
            tracknodes = []
            tracknodes = generate_tracknode(bl_sections, segments, is_point2point)
            print(MSG_INFO_DATA.format( 'nodes len', len(tracknodes) ))
            # TrackPoint
            for node in tracknodes:
                for point in node.points:
                    point.pack(file)
                node.branch_count = len(node.points)
                node.point_ptr = node.points[0].adr
            write_padd(file, 0x100) # this for Develop TODO: Remove this

            # TrackNode
            for node in tracknodes:
                node.pack(file)
            # Update Header
            header.tracknode_count = len(tracknodes)
            header.tracknode_ptr = tracknodes[0].adr
            # Get TotalLength
            total_distance = tracknodes[-1].points[0].distance_end
        write_padd(file, 0x100) # this for Develop TODO: Remove this

        # TotalLength
        track_info_adr = file.tell()
        buff = struct.pack('>1f', total_distance)
        file.write(buff)
        write_padd(file, 0x100) # this for Develop TODO: Remove this
         # Update Header
        header.trackinfo = track_info_adr

        # Data0x1C
        header.data0x1c_count = 0x01
        header.data0x1c_ptr = file.tell()
        buff = struct.pack('>4I', \
                            0x00, 0x00, 0x00, 0x00)
        file.write(buff)

        # Data0x14 DUMMY
        header.data0x14_count = 0x00
        header.data0x14_ptr = file.tell()
        padd_list = [0x00] * 15
        buff = struct.pack('>4f4B15i', \
                            64.0, 1110.84, -0.048339, 0.045849, \
                            0x03, 0x00, 0x00, 0x00, \
                            *padd_list)
        file.write(buff)

        # ?????? DUMMY
        padd_list = [0x00] * 21
        buff = struct.pack('>4f1I21i', \
                            -1055.05, -331.491, 120.063, 140.233, \
                            0x10, \
                            *padd_list)
        file.write(buff)
        
        # 1200 byte
        padd_list = [0x00] * 250
        pad_buff = struct.pack('{0}i'.format(250), *padd_list)
        file.write(pad_buff)

        write_padd(file, 0x100)

        # Loop flag
        header.loop_flag = (0x01 << 0x10) if is_point2point else 0x00

        # Header
        file.seek(0x00)
        header.unk0x04 = 3600.0
        header.unk0xd0 = 0x08
        header.unk0xd4 = 0x08
        header.aabb.a_min = -964.6077
        header.aabb.b_min = -352.0202
        header.aabb.a_max = 248.5846
        header.aabb.b_max = 302.4815
        header.pack(file, is_ax)
    
    file.close()