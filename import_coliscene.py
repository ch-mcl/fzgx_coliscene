import os
import sys

import math
import mathutils
import struct
import bpy

from .header import Header
from .node import TrackNode, TrackSegment

NAME_ROOT_OBJ = '[COLI_COURSE]'
NAME_CHILD = '{0}_{1:02}'
NAME_CHECKPOINT = '[POINT]_{0:04}'
NAME_CHECKPOINT_START = '{0}_in'
NAME_CHECKPOINT_END = '{0}_out'
NAME_ROAD_ROOT = '[ROAD_ROOT]'
NAME_ROAD = '[ROAD]'
NAME_ROAD_ID = '{0}_{1:02}'

MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_DATA_HEX = '{0}: {1:#X}'

#Convert Direction to Matrix Rotation (Generate lookat rotation)
def direction2mtx(direction):
    forward = mathutils.Vector(direction)
    wolrd_up = mathutils.Vector((0, 1, 0))
    left = wolrd_up.cross(forward)
    up = forward.cross(left)
    
    mtx = mathutils.Matrix()
    mtx[0][0] = left.x
    mtx[1][0] = left.y
    mtx[2][0] = left.z
    mtx[0][1] = up.x
    mtx[1][1] = up.y
    mtx[2][1] = up.z
    mtx[0][2] = forward.x
    mtx[1][2] = forward.y
    mtx[2][2]= forward.z
    
    return mtx

#TODO: fix this
def euler2direction(angle):
    angle_rad = [math.radians(angle[0]), math.radians(angle[1]), math.radians(angle[2])]
    euler = mathutils.Euler(angle_rad)
    mtx = euler.to_matrix()
    direction = mathutils.Vector((mtx[0][2], mtx[1][2], mtx[2][2]))
    
    return direction
    
def generate_track_checkpoint(position, tangent, trackWidth, point_name, is_start, child_idx, parent_name):
        #tangent to angle
        
        mtx = direction2mtx(tangent)
        decomp = mtx.decompose()
        rot = decomp[1].to_euler()
        width = trackWidth * 0.5
        #right position
        #right = mathutils.Vector((width, 0, 0))
        #right_pos = decomp[1] * right
        #pos = mathutils.Vector(position) - right_pos
        pos = mathutils.Vector(position)
        
        bpy.ops.object.empty_add(\
                type='ARROWS', \
                radius = width, \
                view_align = False, \
                location = (pos[0], pos[1], pos[2]), \
                rotation = (rot[0], rot[1], rot[2]) \
            )
        name_str = NAME_CHECKPOINT_START if is_start == True else NAME_CHECKPOINT_END
        name = name_str.format(point_name)
        if child_idx > 0:
            name = NAME_CHILD.format(name, child_idx)
            name_str = NAME_CHECKPOINT_START if is_start == True else NAME_CHECKPOINT_END
            parent_name = name_str.format(parent_name)
        bpy.context.object.name = name
        bpy.context.object.parent = bpy.data.objects[parent_name]

# Generate KeyFrame
def generate_tracksegment(transform, idx, parent_name=NAME_ROOT_OBJ):
    # Genarate ROAD (squer)
    bpy.ops.mesh.primitive_cube_add(radius=0.5, \
                                    view_align=False, \
                                    enter_editmode=False, \
                                    location=(0, 0, 0))
    road_obj = bpy.context.object
    road_obj_name = NAME_ROAD_ID.format(NAME_ROAD if parent_name == NAME_ROOT_OBJ else parent_name, idx)
    #print(MSG_INFO_DATA.format('ROAD parent', root_obj_name))
    # [ROAD]_XX parent is [ROAD_ROOT]__XX
    road_obj.parent = bpy.data.objects[parent_name] 
    road_obj.name = road_obj_name
    road_obj.scale = transform.local_scale
    road_obj.rotation_euler = ( math.radians(transform.local_rotation[0]), \
                math.radians(transform.local_rotation[1]), \
                math.radians(transform.local_rotation[2]) )
    road_obj.location = transform.local_position
    road_obj.show_name = True
    
    # initalize Keyframe
    bpy.context.scene.frame_current = 0
    bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_DeltaScale')
    bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_DeltaRotation')
    bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_DeltaLocation')
        
    for i, list in enumerate(transform.topologyParameter.keys_list):
        fcurve = bpy.data.objects[road_obj_name].animation_data.action.fcurves[i]
        for key in list:
            frame = round(key.time * 60)
            ease = key.ease_mode
            value = key.value
            if ( (i == 3) or (i == 4) or ( i == 5) ):
                value = math.radians(key.value)
            value_in = key.z_tangent_in
            value_out = key.z_tangent_out

            fcurve.keyframe_points.insert(frame, value)
            #point = fcurve.keyframe_points[-1]
            ## Left Handle
            #point.handle_left = (point.handle_left[0], value-value_in)
            #point.handle_left_type = 'FREE' # TODO:apply ease value
            ## Right Handle
            #point.handle_right = (point.handle_right[0], value+value_out)
            #point.handle_right_type = 'FREE' # TODO:apply ease value
    
    for i, child in enumerate(transform.children):
        generate_tracksegment(child, i, road_obj_name)
            

#import COLI_COURSE
def load(filepath, is_ax):

    # Generate ROOT Object
    bpy.ops.object.empty_add(\
            type='SPHERE', \
            radius = 100.0, \
            view_align = False, \
            location = (0.0, 0.0, 0.0), \
            rotation = (math.radians(90.0), 0.0 ,0.0) \
        )
    bpy.context.object.name = NAME_ROOT_OBJ
    
    with open(filepath, 'rb') as file:
        
        header = Header()
        header.unpack(file, is_ax)

        print('header unpack')
        print(header.trackNode_count)
        print(MSG_INFO_DATA_HEX.format('trackNode_AbsPtr', header.trackNode_AbsPtr))
            
        print(MSG_INFO_DATA_HEX.format('data0x14_count', header.data0x14_count))
        print(MSG_INFO_DATA_HEX.format('data0x14_AbsPtr', header.data0x14_AbsPtr))


        print(MSG_INFO_DATA_HEX.format('unk0x24', header.unk0x24))
        print(MSG_INFO_DATA_HEX.format('unk0x48', header.unk0x48))
        print(MSG_INFO_DATA_HEX.format('unk0x4C', header.unk0x4C))

        print(MSG_INFO_DATA_HEX.format('gameObject_count', header.gameObject_count))
        # print(MSG_INFO_DATA_HEX.format('gameObject_AbsPtr', header.gameObject_AbsPtr))
        # print(MSG_INFO_DATA_HEX.format('unk0x58', header.unk0x58))

        # print(MSG_INFO_DATA_HEX.format('data0x68_count', header.data0x68_count))

        # print(MSG_INFO_DATA_HEX.format('data0x70_AbsPtr', header.data0x70_AbsPtr))
        # print(MSG_INFO_DATA_HEX.format('loop_flag', header.loop_flag))
        # print(MSG_INFO_DATA_HEX.format('data0x80', header.data0x80))
        # print(MSG_INFO_DATA_HEX.format('data0x84', header.data0x84))

        print(MSG_INFO_DATA_HEX.format('trackInfo', header.trackInfo))

        print(header.aabb.A_min)
        print(header.aabb.B_min)
        print(header.aabb.A_max)
        print(header.aabb.B_max)

        print(MSG_INFO_DATA_HEX.format('unk0xD0', header.unk0xD0))
        print(MSG_INFO_DATA_HEX.format('unk0xD4', header.unk0xD4))
        print(MSG_INFO_DATA.format('unk0xFC', header.unk0xFC))
        
        # TrackNode
        address = file.tell()
        file.seek(header.trackNode_AbsPtr)
        nodes = []
        for i in range(header.trackNode_count):
            node = TrackNode()
            node.unpack(file)
            nodes.append(node)

        print(len(nodes))


        track_segment_absptrs = []
        for i, node in enumerate(nodes):
            parent_name = NAME_ROOT_OBJ
            point_name = NAME_CHECKPOINT.format(i)
            for child_idx, point in enumerate(node.points):
                # Generate Start
                is_start = True
                generate_track_checkpoint(point.position_start, point.tangent_start, point.width, point_name, is_start, child_idx, parent_name)
                # Generate End
                is_start = False
                generate_track_checkpoint(point.position_end, point.tangent_end, point.width, point_name, is_start, child_idx, parent_name)
                
                # Maybe parent is point[0] objects
                parent_name = point_name
            
            segment_absptr = node.segment_ptr
            if ( not(segment_absptr in track_segment_absptrs) ):
                track_segment_absptrs.append(segment_absptr)
                
        print('TrackSegment count')
        print( len(track_segment_absptrs) )

        # Parse TrackSegment
        segments = []
        for track_transform_absptr in track_segment_absptrs:
            file.seek(track_transform_absptr)
            segment = TrackSegment()
            segment.unpack(file)
            segments.append(segment)
        # Generate TrackSegment
        for i, segment in enumerate(segments):
            generate_tracksegment(segment, i)

        #print ('function test')
        #vec_print = 'X:{0:f} Y:{1:f} Z:{2:f}'
        #rot = [0, 90, 0]
        #tan = euler2direction(rot)
        #print( vec_print.format(tan[0], tan[1], tan[2]) )
        #pos = mathutils.Vector((1145.14, 0, 0))
        #generate_track_point(pos, tan, 26.0, 'FUNC_TEST', True, 0, '[POINT_ROOT]')

        #rot = [0, 0, 60]
        #tan = euler2direction(rot)
        #pos = mathutils.Vector((1145.14, 0, 114.514))
        #print( vec_print.format(tan[0], tan[1], tan[2]) )
        #generate_track_point(pos, tan, 26.0, 'FUNC_TEST', False, 0, '[POINT_ROOT]')
    file.close()
    

# #must to set ColiScene ID
#filename = r"01_MCTR\COLI_COURSE01,lz"
# #filename = r"03_MCSG\COLI_COURSE03,lz"
# #filename = r"05_AM\COLI_COURSE05,lz"
#filepath = r"D:\Hack\dolphin\F-ZEROGX\ReverseEngineer\COLI\OFFICIAL\\" + filename

#load(filepath)