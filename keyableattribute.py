import os
import sys

from enum import Enum
import struct

#Messagess
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_TODO = 'Do Something {0}'

#InterpolationMode
class InterpolationMode(Enum):
        Constant = 0
        Linear = 1
        unknown1 = 2 # could be auto or auto clamped?
        unknown2 = 3 # could be auto or auto clamped?
    

#Keyable Attribute
class KeyableAttribute :
    fmt = '>1I4f'
    # 1I  4f
    # 0,  1-4
    
    def __init__(self):
        self.ease_mode = InterpolationMode.Constant
        self.time = 0.0
        self.value = 0.0
        self.z_tangent_in = 0.0
        self.z_tangent_out = 0.0
        
    #Unpack
    def unpack(self, file):
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        self.ease_mode = InterpolationMode(buff[0])
        self.time = buff[1]
        self.value = buff[2]
        self.z_tangent_in = buff[3]
        self.z_tangent_out = buff[4]
    
    #Pack
    def pack(self, file):
        ease = int(self.ease_mode.value)
        buff = struct.pack(self.fmt, \
                    ease, self.time, self.value, self.z_tangent_in, \
                    self.z_tangent_out)
        file.write(buff)