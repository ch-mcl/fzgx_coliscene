import os
import sys

from ctypes import *
import numpy
import mathutils
import struct

#Messages
#MSG_WARN_NOT_GCMF = 'magic_str:{0} not match magic:{1}'
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_TODO = 'Do Something {0}'

    
class AABB:
    fmt = '>4f'
    # 4f
    # 0-3
    
    def __init__(self):
        self.a_min = 0.0
        self.b_min = 0.0
        self.a_max = 0.0
        self.b_max = 0.0

    #Unpack
    def unpack(self, file, offset):
        address = file.tell()
        file.seek(offset)
        
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        self.a_min = buff[0]
        self.b_min = buff[1]
        self.a_max = buff[2]
        self.b_max = buff[3]
    
    #Pack
    def pack(self, file):
        buff = struct.pack(self.fmt, \
                    self.a_min, self.b_min, \
                    self.a_max, self.b_max)
        file.write(buff)

#Header
class Header:
    fmt_GX = '>'+'2f1i2I1i1I1i'+'2i6I'+'2I3i1I1i1I'+'2i1I1i1I2I1i'+'2I3I1i1I1i'+'1I1i1I1i1I1i2I'+'4f2i2I'+'7I1f'
    # 0x00-0x1C
    # 2f   1i 2I   1i 1I 1i
    # 0-1, 2, 3-4, 5, 6, 7
    # 0x20-0x3C
    # 2i   6I
    # 8-9, 10-15
    # 0x40-0x5C
    # 2I     3i        1I  1i  1I
    # 16-17, 18-19,20, 21, 22, 23
    # 0x60-0x7C
    # 2i     1I  1i  1I  2I     1i 
    # 24-25, 26, 27, 28, 29-30, 31
    # 0x80-0x9C
    # 2I     3I         1i  1I  1i
    # 32-33. 34-35, 36, 37, 38, 39
    # 0xA0-0xBC
	# 1I  1i  1I  1i  1I  1i  2I
    # 40, 41, 42, 43, 44, 45, 46-47
	# 0xC-0xDC
    # 4f     2i    2I
	# 48-51, 52-53 54-55
    # 0xE0-0xFC
    # 7I    1f
    # 56-62 63
    
    fmt_AX = '>'+'2f1i2I1i1I1i'+'2i6I'+'2I2i1I1i1I'+'2i1I1i1I2I1i'+'2I3I1i1I1i'+'1I1i1I1i1I1i2I'+'4f2i2I'+'7I1f'
    # different from GX
    # 0x40-0x5C
    # 2I     2i     1I  1i  1I
    # 16-17, 18-19, 20, 21, 22
    # other is same as GX

    def __init__(self, is_ax=False):
        self.unk0x00 = 0.0
        self.unk0x04 = 0.0
        self.tracknode_count = 0x00
        self.tracknode_ptr = 0x00
        
        self.data0x14_count = 0x00
        self.data0x14_ptr = 0x00
        self.data0x1c_count = 0x00
        self.data0x1c_ptr = 0x00
        
        self.unk0x20 = 0xE4 if is_ax == True else 0xE8
        self.unk0x24 = 0xF8 if is_ax == True else 0xFC
        # 0x28 ~ 0x44 is NULL
        self.unk0x48 = 0x00
        self.unk0x4c = 0x00
        
        self.sceneobject_count = 0x00
        self.sceneobject_ptr = 0x00
        self.unk0x58 = 0x00 # const 1
        self.unk0x60 = 0x00 # SOLS only
        self.data0x68_count = 0x00
        self.data0x68_ptr = 0x00
        self.data0x70_count = 0x00
        self.data0x70_ptr = 0x00
        
        self.loop_flag = 0x00
        self.data0x80 = 0x00
        self.data0x84 = 0x00
        
        self.trackinfo = 0x00
        self.data0x98_count = 0x00
        self.data0x98_ptr = 0x00 # Animation 1
        self.data0xa0_count = 0x00
        self.data0xa0_ptr = 0x00 # Animation 2
        self.data0xa8_count = 0x00
        self.data0xa8_ptr = 0x00 # AX checkPoint
        self.data0xb0_count = 0x00
        self.data0xb0_ptr = 0x00 # Object Path
        self.data0xb8_count = 0x00
        self.data0xb8_ptr = 0x00 # Story Mode Object
        self.data0xbc_ptr = 0x00
        self.aabb = AABB()
        self.unk0xd0 = 0x08 # const 8
        self.unk0xd4 = 0x08 # const 8
        
        self.unk0xFC = 0.0

    #Unpack
    def unpack(self, file, is_ax):
        fmt = self.fmt_AX if is_ax == True else self.fmt_GX
        bytes = file.read(struct.calcsize(fmt))
        buff = struct.unpack_from(fmt, bytes, 0)
        
        print(len(buff))
        print('{0:#X}'.format(struct.calcsize(fmt)))

        
        self.unk0x00 = buff[0]
        self.unk0x04 = buff[1]
        self.tracknode_count = buff[2]
        self.tracknode_ptr = buff[3]
        #--
        self.data0x14_count = buff[4]
        self.data0x14_ptr = buff[5]
        self.data0x1c_count = buff[6]
        self.data0x1c_ptr = buff[7]
        #--
        self.unk0x20 = buff[8]
        self.unk0x24 = buff[9] #OK
        #10-17
        #
        self.unk0x48 = buff[18]

        idx = 19
        if is_ax != True:
            self.unk0x4c = buff[idx] # GX:19 AX:--
            idx += 1
        
        self.sceneobject_count = buff[idx] # GX:20 AX:19
        idx += 1
        self.sceneobject_ptr = buff[idx] # GX:21 AX:20
        idx += 1
        self.unk0x58 = buff[idx] # GX:22 AX:21
        idx += 2
        #GX:23 AX:22
        #--
        self.unk0x60 = buff[idx] # GX:24 AX:23
        idx += 1
        self.data0x68_count = buff[idx] # GX:25 AX:24
        idx += 1
        self.data0x68_ptr = buff[idx] # GX:26 AX:25
        idx += 1
        self.data0x70_count = buff[idx] # GX:27 AX:26
        idx += 1
        self.data0x70_ptr = buff[idx] # GX:28 AX:27
        idx += 3
        #GX:29-30 AX:28-29
        self.loop_flag = buff[idx] # GX:31 AX:30
        idx += 1
        #--
        self.data0x80 = buff[idx] # GX:32 AX:31
        idx += 1
        self.data0x84 = buff[idx] # GX:33 AX:32
        idx += 3
        #GX:34-35 AX:33-34
        self.trackinfo = buff[idx] # GX:36 AX:35
        idx += 1
        self.data0x98_count = buff[idx] # GX:37 AX:36
        idx += 1
        self.data0x98_ptr = buff[idx] # GX:38 AX:37
        idx += 1
        self.data0xa0_count = buff[idx] # GX:39 AX:38
        idx += 1
        #--
        self.data0xa0_ptr = buff[idx] # GX:40 AX:39
        idx += 1
        self.data0xa8_count = buff[idx] # GX:41 AX:40
        idx += 1
        self.data0xa8_ptr = buff[idx] # GX:42 AX:41
        idx += 1
        #--
        self.data0xb0_count = buff[idx] # GX:43 AX:42
        idx += 1
        self.data0xb0_ptr = buff[idx] # GX:44 AX:43
        idx += 1
        self.data0xb8_count = buff[idx] # GX:45 AX:44
        idx += 1
        self.data0xb8_ptr = buff[idx] # GX:46 AX:45
        idx += 1
        self.data0xbc_ptr = buff[idx] # GX:47 AX:46
        idx += 1
        #--
        self.aabb.A_min = buff[idx] # GX:48 AX:47
        idx += 1
        self.aabb.B_min = buff[idx] # GX:49 AX:48
        idx += 1
        self.aabb.A_max = buff[idx] # GX:50 AX:49
        idx += 1
        self.aabb.B_max = buff[idx] # GX:51 AX:50
        idx += 1
        #--
        self.unk0xd0 = buff[idx] # GX:52 AX:51
        idx += 1
        self.unk0xd4 = buff[idx] # GX:53 AX:52
        idx += 10
        #GX:54-55 AX:53-54
        #--
        #GX:56-62 AX:55-61
        self.unk0xFC = buff[idx] # GX:63 AX:62
    
    def pack (self, file, is_ax):
        if is_ax == True:
            # AX
            # TODO finish this
            # padd_list = [0x00] * 62
            # buff = struct.pack(self.fmt_AX, *padd_list)
            buff = struct.pack(self.fmt_AX, \
                        self.unk0x00, self.unk0x04, self.tracknode_count, self.tracknode_ptr, \
                        self.data0x14_count, self.data0x14_ptr, self.data0x1c_count, self.data0x1c_ptr, \
                        self.unk0x20, self.unk0x24, 0x00, 0x00, \
                        0x00, 0x00, 0x00, 0x00, \
                        0x00, 0x00, self.unk0x48, self.sceneobject_count, \
                        self.sceneobject_ptr, self.unk0x58, 0x00, self.unk0x60, \
                        self.data0x68_count, self.data0x68_ptr, self.data0x70_count, self.data0x70_ptr, \
                        0x00, 0x00, self.loop_flag, self.data0x80, \
                        self.data0x84, 0x00, 0x00, self.trackinfo,\
                        self.data0x98_count, self.data0x98_ptr, self.data0xa0_count, self.data0xa0_ptr, \
                        self.data0xa8_count, self.data0xa8_ptr, self.data0xb0_count, self.data0xb0_ptr, \
                        self.data0xb8_count, self.data0xb8_ptr, self.data0xbc_ptr, self.aabb.a_min, \
                        self.aabb.b_min, self.aabb.a_max, self.aabb.b_max, self.unk0xd0, \
                        self.unk0xd4, 0x00, 0x00, 0x00, \
                        0x00, 0x00, 0x00, 0x00, \
                        0x00, 0x00, self.unk0xFC)
        else:
            # GX
            buff = struct.pack(self.fmt_GX, \
                        self.unk0x00, self.unk0x04, self.tracknode_count, self.tracknode_ptr, \
                        self.data0x14_count, self.data0x14_ptr, self.data0x1c_count, self.data0x1c_ptr, \
                        self.unk0x20, self.unk0x24, 0x00, 0x00, \
                        0x00, 0x00, 0x00, 0x00, \
                        0x00, 0x00, self.unk0x48, self.unk0x4c, \
                        self.sceneobject_count, self.sceneobject_ptr, self.unk0x58, 0x00, \
                        self.unk0x60, self.data0x68_count, self.data0x68_ptr, self.data0x70_count, \
                        self.data0x70_ptr, 0x00, 0x00, self.loop_flag, \
                        self.data0x80, self.data0x84, 0x00, 0x00, \
                        self.trackinfo, self.data0x98_count, self.data0x98_ptr, self.data0xa0_count, \
                        self.data0xa0_ptr, self.data0xa8_count, self.data0xa8_ptr, self.data0xb0_count, \
                        self.data0xb0_ptr, self.data0xb8_count, self.data0xb8_ptr, self.data0xbc_ptr, \
                        self.aabb.a_min, self.aabb.b_min, self.aabb.a_max, self.aabb.b_max, \
                        self.unk0xd0, self.unk0xd4, 0x00, 0x00, \
                        0x00, 0x00, 0x00, 0x00, \
                        0x00, 0x00, 0x00, self.unk0xFC)
        file.write(buff)