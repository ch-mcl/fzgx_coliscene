import os
import sys

from enum import Enum
import struct

#Messagess
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_TODO = 'Do Something {0}'

#FiXed-int coefficient
fx = 8191.0

class Base:
    adr = 0x00


# SceneObjectLOD
class SceneObjectLOD(Base):
    fmt = '>3I1f'
    def __init__(self):
            self.unk_0x00 = 0x00
            self.lod_name_ptr = 0x00
            self.unk_0x08 = 0x00
            self.lod_distance = 0.0

    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        self.unk_0x00 = buff[0]
        self.lod_name_ptr = buff[1]
        self.unk_0x08 = buff[2]
        self.lod_distance = buff[3]

    # Pack
    def pack(self, file):
        self.adr = file.tell()
        buff = struct.pack(self.fmt, \
                    self.unk_0x00, self.lod_name_ptr, \
                    self.unk_0x08, self.lod_distance)
        file.write(buff)


# SceneObject
class SceneObject(Base):
    fmt = '>4I'
    def __init__(self):
        self.unk_0x00 = 0x00
        self.lod_count = 0x00
        self.lod_ptr = 0x00
        self.collider_geometry_ptr = 0x00
        
        self.lods = []

    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        self.unk_0x00 = buff[0]
        self.lod_count = buff[1]
        self.lod_ptr = buff[2]
        self.collider_geometry_ptr = buff[3]

    # Pack
    def pack(self, file):
        self.adr = file.tell()
        buff = struct.pack(self.fmt, \
                    self.unk_0x00, self.lod_count, \
                    self.lod_ptr, self.collider_geometry_ptr)
        file.write(buff)


# TransformPRXS
class TransformPRXS(Base):
    def __init__(self):
        self.position = [0.0, 0.0, 0.0]
        self.decomposed_rotation = [0.0, 0.0, 0.0]
        self.unknown_option = 0x00
        self.object_active_override = 0x00
        self.scale = [0.0, 0.0, 0.0]


# TransformMatrix3x4
class TransformMatrix3x4(Base):
    fmt = '>12f'
    def __init__(self):
        self.mtx = [ \
            [0.0, 0.0, 0.0, 0.0], \
            [0.0, 0.0, 0.0, 0.0], \
            [0.0, 0.0, 0.0, 0.0] ]

    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.mtx[0][0:4] = buff[0:4]
        self.mtx[1][0:4] = buff[4:8]
        self.mtx[2][0:4] = buff[9:12]

    # Pack
    def pack(self, file):
        self.adr = file.tell()
        buff = struct.pack(self.fmt, \
                    *self.mtx[0][0:4], \
                    *self.mtx[1][0:4], \
                    *self.mtx[2][0:4])
        file.write(buff)

# SceneObjectDynamic
class SceneObjectDynamic(Base):
    fmt = '>3I3f3H2B3f5I'
    # 3I   3f   3H   2B    3f,    5I
    # 0-2, 3-5, 6-8, 9-10, 11-13, 14-18

    def __init__(self):
        self.unk0x00 = 0x00 # unpack needs "sqrt", pack needs "pow"
        self.unk0x04 = 0x00 # unpack needs "sqrt", pack needs "pow"
        self.scene_object_ptr = 0x00
        self.transform_prxs = TransformPRXS()

        self.animation_ptr = 0x00
        self.texture_scroll_ptr = 0x00
        self.skeletal_animator_ptr = 0x00
        self.transform_ptr = 0x00

        self.transform_mtx = TransformMatrix3x4()

    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        self.unk0x00 = buff[0]
        self.unk0x04 = buff[1]
        self.scene_object_ptr = buff[2]

        transform_prxs = TransformPRXS()
        transform_prxs.position = [buff[3], buff[4], buff[5]]
        # TODO: convert u16 rotate to float
        rot = [ \
            buff[6], \
            buff[7], \
            buff[8]]
        transform_prxs.decomposed_rotation = rot
        transform_prxs.unknown_option = buff[9]
        transform_prxs.object_active_override = buff[10]
        transform_prxs.scale = [buff[11], buff[12], buff[13]]
        self.transform_prxs = transform_prxs
        # 14
        self.animation_ptr = buff[15]
        self.texture_scroll_ptr = buff[16]
        self.skeletal_animator_ptr = buff[17]
        self.transform_ptr = buff[18]

        address = file.tell()
        file.seek(self.transform_ptr)
        self.transform_mtx.unpack(file)
        file.seek(address)
    
    # Pack
    def pack(self, file):
        self.adr = file.tell()
        _unk0x00 = self.unk0x00
        _unk0x04 = self.unk0x04

        # TODO: Convert to shortAngles
        # rot = [int(self.transform.decomposed_rotation[0]), \
        #         int(self.transform.decomposed_rotation[1]), \
        #         int(self.transform.decomposed_rotation[2])]
        rot = [0x00, 0x00, 0x00]
        buff = struct.pack(self.fmt, \
                    _unk0x00, _unk0x04, self.scene_object_ptr, \
                    *self.transform_prxs.position, \
                    *rot, self.transform_prxs.unknown_option, self.transform_prxs.object_active_override, \
                    *self.transform_prxs.scale, 0x00, \
                    self.animation_ptr, self.texture_scroll_ptr, self.skeletal_animator_ptr, self.transform_ptr)
        file.write(buff)