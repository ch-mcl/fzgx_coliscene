import os
import sys

import struct

from .keyableattribute import KeyableAttribute

# Messages
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_DATA_HEX = '{0}: {1:#X}'
MSG_INFO_TODO = 'Do Something {0}'

# Write Padding
def write_padd(file, size):
    # padding (aligned by "size")
    modded = (file.tell() % size)
    if (modded != 0):
        pad_len = size - modded
        padd_list = [0x00] * pad_len
        pad_buff = struct.pack('{0}B'.format(pad_len), *padd_list)
        file.write(pad_buff)


class Base:
    adr = 0x00


class Plane(Base):
    def __init__(self):
        self.dotProduct = 0.0
        self.normal = [0.0, 0.0, 0.0]
        self.origin = [0.0, 0.0, 0.0]


# CheckPoint
class CheckPoint(Base):
    fmt = '>19f1I'
    # 19f   1I
    # 0~18, 19
    
    def __init__(self):
        self.curve_time_start = 0.0
        self.curve_time_end = 0.0
        self.plane_start = Plane()
        self.plane_end = Plane()
        self.distance_start = 0.0
        self.distance_end = 0.0
        self.width = 0.0
        self.connect_to_track_in = False
        self.connect_to_track_out = False

    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        self.curve_time_start = buff[0]
        self.curve_time_end = buff[1]

        plane_start = Plane()
        plane_start.dotProduct = buff[2]
        plane_start.normal = [ buff[3], buff[4], buff[5] ]
        plane_start.origin = [ buff[6], buff[7], buff[8] ]
        self.plane_start = plane_start

        plane_end = Plane()
        plane_end.dotProduct = buff[9]
        plane_end.normal = [ buff[10], buff[11], buff[12] ]
        plane_end.origin = [ buff[13], buff[14], buff[15] ]
        self.plane_end = plane_end
        
        self.distance_start = buff[16]
        self.distance_end = buff[17]
        self.width = buff[18]
        flag = buff[19]
        if flag>>0x18 & 0x01 == 1:
            self.connect_to_track_in = True
        if flag>>0x10 & 0x01 == 1:
            self.connect_to_track_out = True

    #Pack
    def pack(self, file):
        self.adr = file.tell()
        flag = int(0x00)
        if self.connect_to_track_in == True:
            flag = flag + (0x01 << 0x18)
        if self.connect_to_track_out == True:
            flag = flag + (0x01 << 0x10)

        buff = struct.pack(self.fmt, \
                    self.curve_time_start, self.curve_time_end, \
                    self.plane_start.dotProduct, *self.plane_start.normal, *self.plane_start.origin, \
                    self.plane_end.dotProduct, *self.plane_end.normal, *self.plane_end.origin, \
                    self.distance_start, self.distance_end, self.width, \
                    flag)
        file.write(buff)
        

# TrackCurves
class TrackCurves(Base):
    fmt = '>18i'
    #Scale X, Y, Z
    #Rotation X, Y, Z
    #Translation X, Y, Z
    
    def __init__(self):
        self.key_count = [ \
                            0x00, 0x00, 0x00, \
                            0x00, 0x00, 0x00, \
                            0x00, 0x00, 0x00 ]
        self.key_ptrs = [ \
                            0x00, 0x00, 0x00, \
                            0x00, 0x00, 0x00, \
                            0x00, 0x00, 0x00 ]
        self.keys_list = [\
                        [], [], [], \
                        [], [], [], \
                        [], [], [] ]
        
    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        address = file.tell()
        for i, keys in enumerate(self.keys_list):
            self.key_count[i] = buff[i]
            self.key_ptrs[i] = buff[i+9]
            file.seek(self.key_ptrs[i])
            for j in range(self.key_count[i]):
                key = KeyableAttribute()
                key.unpack(file)
                keys.append(key)
        
        file.seek(address)
        
    # Pack
    def pack(self, file):
        # Write KeyableAttribute
        for i, keys in enumerate(self.keys_list):
            self.key_count[i] = len(keys)
            self.key_ptrs[i] = file.tell() if self.key_count[i] > 0 else 0x00
            for key in keys:
                key.pack(file)

        # Padding TODO:Remove
        write_padd(file, 0x10)

        # Write TrackCurves
        self.adr = file.tell()
        buff = struct.pack(self.fmt, \
                    *self.key_count, \
                    *self.key_ptrs)
        file.write(buff)

        # Padding TODO:Remove
        write_padd(file, 0x10)


# TrackSegment
class TrackSegment(Base):
    fmt = '>4B1I2i1I9f1i2f3I'
    # 4B   1I 2i   1I 9f    1i, 2f     3I
    # 0-3, 4, 5-6, 7, 8-16, 17, 18-19, 20-22
    
    def __init__(self):
        #hierarchyDepth = TrackTransformHierarchyDepth()
        self.hierarchy_depth = 0x00
        self.zero_0x01 = 0x00
        self.has_children = 0x00
        self.zero_0x03 = 0x00
        self.track_curves_ptr = 0x00
        self.unk_0x08_ptr  = 0x00
        #extraTransform = ExtraTransform()
        
        self.child_count = 0x00
        self.children_ptr = 0x00
        
        self.local_scale = [ 0.0, 0.0, 0.0 ]
        self.local_rotation = [ 0.0, 0.0, 0.0 ]
        self.local_position = [ 0.0, 0.0, 0.0 ]
        self.unk_0x38 = 0x00
        self.unk_0x3c = 0x00
        self.unk_0x40 = 0x00
        self.zero_0x44 = 0x00
        self.zero_0x48 = 0x00
        self.unk_0x4c = 0x00
        
        self.track_curves = TrackCurves()
        self.children = []
        
    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        # print(MSG_INFO_DATA_HEX.format('TrackSegment', file.tell()))
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        address = file.tell()
        
        self.hierarchy_depth = buff[0]
        #self.hierarchyDepth.unpack(buff[0])
        #1
        self.has_children = buff[2]
        #3
        self.track_curves_ptr = buff[4]
        self.unk_0x08_ptr = buff[5]
        #extraTransform = ExtraTransform()

        self.child_count = buff[6]
        self.children_ptr = buff[7]

        self.local_scale = [ buff[8], buff[9], buff[10] ]
        self.local_rotation = [ buff[11], buff[12], buff[13] ]
        self.local_position = [ buff[14], buff[15], buff[16] ]
        self.unk_0x38 = buff[17]
        self.unk_0x3c = buff[18]
        self.unk_0x40 = buff[19]
        self.zero_0x44 = buff[20]
        self.zero_0x48 = buff[21]
        self.unk_0x4c = buff[22]
        
        file.seek(self.track_curves_ptr)
        self.track_curves.unpack(file)
        
        if (self.child_count > 0):
            file.seek(self.children_ptr)
            for i in range(self.child_count):
                child = TrackSegment()
                child.unpack(file)
                self.children.append(child)
            
        file.seek(address)

    # Pack TrackCurves
    def pack_trackcurves(self, file):
        self.track_curves.pack(file)
        self.track_curves_ptr = self.track_curves.adr

        for child in self.children:
            child.track_curves_ptr = file.tell()
            child.pack_trackcurves(file)

    # PickUp children
    def pickup_child(self, all_childs, insert_index, depth):
        for child in self.children:
            insert_trans = insert_index[depth]
            if (insert_trans != None):
                # Get Index of Inserting Transform
                idx = all_childs.index(insert_trans)
                idx += 1
                if (idx > len(all_childs)):
                    # over maximum of all_childs index
                    # -> "Add" TrackSegment
                    all_childs.append(child)
                else:
                    # not over maximum of all_childs index
                    # -> "Insert" TrackSegment
                    all_childs.insert(idx, child)
            else:
                # First time of Child inserting in all_childs 
                # "Add" TrackSegment
                all_childs.append(child)

            # Update Insert Index Object
            insert_index[depth] = child
            # Recursive (depth increase)
            child.pickup_child(all_childs, insert_index, depth+1)

    # Pack themself TrackSegment only
    def pack(self, file):
        self.adr = file.tell()
        buff = struct.pack(self.fmt, \
                    self.hierarchy_depth, self.zero_0x01, self.has_children, self.zero_0x03, \
                    self.track_curves_ptr, self.unk_0x08_ptr, self.child_count, \
                    self.children_ptr, *self.local_scale, \
                    *self.local_rotation, *self.local_position, \
                    self.unk_0x38, self.unk_0x3c, \
                    self.unk_0x40, self.zero_0x44, self.zero_0x48, self.unk_0x4c)
        file.write(buff)

    # Pack TrackSegment and TrackCurves
    # Seraching Maximum depth is 8
    def pack_root(self, file):
        # Pack TrackCurves (include children too)
        self.pack_trackcurves(file)

        # PickUp children
        all_childs = [] # Collecting TrackSegment(include children)
        insert_index = [None] * 0x08 # Collecting list's insert position index objects
        self.pickup_child(all_childs, insert_index, 0)

        start_adr = file.tell()
        for child in all_childs:
            count = len(child.children)
            child.child_count = count
            if (count > 0x00):
                offs =  all_childs.index(child.children[0]) * struct.calcsize(self.fmt)
                child.children_ptr = start_adr + offs
            child.pack(file)
        
        # Pack Themself
        count = len(self.children)
        self.child_count = count
        if (count > 0x00):
            self.children_ptr = self.children[0].adr
        self.pack(file)

        
# Track Node
class TrackNode(Base):
    fmt = '>3I'
    # 3I
    # 0 ~ 2
    
    def __init__(self):
        self.branch_count = 0x00
        self.point_ptr = 0x00
        self.segment_ptr = 0x00
        self.points = []
    
    # Unpack
    def unpack(self, file):
        self.adr = file.tell()
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.branch_count = buff[0]
        self.point_ptr = buff[1]
        self.segment_ptr = buff[2]
        
        address = file.tell()
        file.seek(self.point_ptr)
        for i in range(self.branch_count):
            point = CheckPoint()
            point.unpack(file)
            self.points.append(point)
        
        file.seek(address)
    
    # Pack
    def pack(self, file):
        self.adr = file.tell()
        buff = struct.pack(self.fmt, \
                    self.branch_count, self.point_ptr, self.segment_ptr)
        file.write(buff)