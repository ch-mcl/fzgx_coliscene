# F-ZERO GX/AX COLICOURSE Format for Blender
support Blender **2.79**

# Blender Scene(on 2.79)/Collection(on 2.8) hierarchy

Import not works!
this Branch Focus on "Export" functions.

<pre>
[ROOT]                              // root of ColiScene.(rotated X-axis 90deg)
  |- [ROAD]                         // root of TrackNode.
  |     |- [SEC]_000                // Collection of TrackSegment. (shorted:'SECTION')
  |     |    |- 000_000             // CheckPoint object values.
  |     |    |- 000_001
  |     |    |- ...
  |     |    |- 000_(n-1)
  |     |    |- 000_n
  |     |    |- [TRANS]_000         // TrackSegment object value. (shorted:'TRANSFORM')
  |     |    |    |- [ANIM]_000     // TrackCurves object values. allows to not exist. (shorted:'ANIMATION')
  |     | 
  |     |- [SEC]_001
  |     |    |- 001_000
  |     |    |    |- 001_000_000    // Branch
  |     |    |    |- 001_000_001
  |     |    |
  |     |    |- 001_001
  |     |    |    |- 001_001_000    // Branch
  |     |    |    |- 001_001_001
  |     |    |- ...
  |     |    |- 001_(n-1)
  |     |    |    |- 001_(n-1)_000  // Branch
  |     |    |    |- 001_(n-1)_001
  |     |    |
  |     |    |- 001_n
  |     |    |    |- 001_n_000      // Branch
  |     |    |    |- 001_n_001
  |     |    |
  |     |    |- [TRANS]_001         // TrackSegment object value. (Transform)
  |     |    |    |- [ANIM]_001
  |     |    |    |- [SEC]_001_000  // child of TrackSegment object value. adding _XXX after *name of parent object*
  |     |    |    |   |- [TRANS]_001_000
  |     |    |    |        |- [ANIM]_001_000
  |     |    |    |
  |     |    |    |- [SEC]_001_001
  |     |    |    |- [SEC]_001_002
  |     | 
  |     |- [SEC]_002
  |     |- ...
  |     |- [SEC]_(n-1)
  |     |- [SEC]_n
  |
  |- [SCENE]                        // root of SceneObjectDynamic.
  |     |- [BIND]                   // root of SceneObjectLOD.
  |     |    |- [000]_*name*        // SceneObjectLOD object value.
  |     |    |
  |     |    |- [001]_*name*        // SceneObjectLOD object (Multiple) value.
  |     |    |    |- *name*         // child
  |     |    |    |- *name*         // child
  |     |    |
  |     |    |- [(n-1))]_*name*
  |     |    |- [(n)]_*name*
  |     |
  |     |- [NAMES]                  // Store list of SceneObjectLOD's names. CustomTrack NOT need this. (Only Generate at Official COLI_COURSE import.) sorted by COLICOURSE's offset. Used at SceneObjectDynamic's SceneObject Export. these string contains ".00n" string. 
  |     |
  |     |- [OBJ]_000                // SceneObjectDynamic object values. also apply Transform and Matrix object values (shorted:'OBJECT')
  |     |    |- [000]_*name*.001    // SceneObjectLOD object value. Linked Duplicate from [BIND]'s [000]_*name*.
  |     |
  |     |- [OBJ]_001
  |     |    |- [000]_*name*.002    // Linked Duplicate from [BIND]'s [000]_*name*.
  |     |
  |     |- [OBJ]_002
  |     |    |- [001]_*name*.001    // Linked Duplicate from [BIND]'s [001]_*name*.
  |     |
  |     |- ...
  |     |- [OBJ]_(n-1)
  |     |- [OBJ]_n
  |
</pre>


ex. st01 (MuteCity TwistRoad)
<pre>
[ROOT]
  |- [ROAD]
  |     |- [SEC]_000
  |     |    |- 000_000
  |     |    |- 000_001
  |     |    |- ...
  |     |    |- 000_029
  |     |    |- 000_030
  |     |    |- [TRANS]_000
  |     |    |    |- [ANIM]_000
  |     | 
  |     |- [SEC]_001
  |     |    |- 001_000
  |     |    |- 001_001
  |     |    |- ...
  |     |    |- 001_061
  |     |    |- 001_062
  |     |    |- [TRANS]_001
  |     |    |    |- [ANIM]_001
  |     | 
  |     |- [SEC]_002
  |     |    |- 002_000
  |     |    |- 002_001
  |     |    |- ...
  |     |    |- 002_013
  |     |    |- 002_014
  |     |    |- [TRANS]_002
  |     |    |    |- [ANIM]_002
  |     | 
  |     |- [SEC]_003
  |     |    |- 003_000
  |     |    |- 003_001
  |     |    |- ...
  |     |    |- 003_024
  |     |    |- 003_025
  |     |    |- [TRANS]_003
  |     |    |    |- [ANIM]_003
  |     | 
  |     |- [SEC]_004
  |     |    |- 003_000
  |     |    |- 003_001
  |     |    |- ...
  |     |    |- 003_028
  |     |    |- 003_029
  |     |    |- [TRANS]_004
  |     |    |    |- [ANIM]_004
  |     | 
  |     |- [SEC]_005
  |     |    |- 005_000
  |     |    |- 005_001
  |     |    |- ...
  |     |    |- 005_020
  |     |    |- 005_021
  |     |    |- [TRANS]_005
  |     |    |    |- [ANIM]_005
  |     |    |    |- [SEC]_005_000
  |     |    |    |    |- [ANIM]_005_000
  |     |    |    |    |- [SEC]_005_000_000
  |
  |- [SCENE]
  |     |- [BIND]
  |     |    |- [000]_DASH01_BOARD
  |     |    |
  |     |    |- [001]_DASH01_SIGN_CIRCLE_A_LOD
  |     |    |    |- ~NULL_NAME~
  |     |    |    |- ~NULL_NAME~.001
  |     |    |
  |     |    |- ...
  |     |    |- [419]_*name*
  |     |    |- [420]_*name*
  |     |
  |     |- [NAMES]
  |     |    |- [000]_DASH01
  |     |    |- [001]_FC010_Pwl6wr6Z02Z.001
  |     |    |- [002]_FC020_Pwl6wr6.001
  |     |    |- ...
  |     |    |- [0XX]_C01_GUARDRAIL01
  |     |    |- ...
  |     |    |- [0XX]_AAA
  |     |    |- [0XX]_~NULL~                  // Special string. this means name is export as null.
  |     |    |- [0XX]_TOWER01_A_LOD
  |     |    |- [0XX]_GROBJ_C_LOD
  |     |    |- [0XX]_GROBJ_B_LOD.001
  |     |    |- [0XX]_GROBJ_A_LOD.001
  |     |    |- ...
  |     |    |- [0XX]_MUT_GROUND_A.001
  |     |    |- ...
  |     |    |- [0XX]_DASH01_BOARD.001
  |     |    |- ...
  |     |    |- [0XX]_DASH01_SIGN_CIRCLE_A_LOD.001
  |     |    |- ...
  |     |    |- [0XX]_STADIUM_OUTWALL_A_LOD.001
  |     |    |- [0XX]_BUILDING_B_LOD.001
  |     |    |- [0XX]_STADIUM_OUTWALL_C_LOD.001
  |     |
  |     |- [OBJ]_000
  |     |    |- [000]_DASH01_BOARD.001      // Linked Duplicate from [BIND]'s [COLI]_000.
  |     |
  |     |- [OBJ]_001
  |     |    |- [000]_DASH01_BOARD.002      // Linked Duplicate from [BIND]'s [COLI]_000.
  |     |
  |     |- [OBJ]_002
  |     |    |- [000]_DASH01_BOARD.003      // Linked Duplicate from [BIND]'s [COLI]_000.
  |     |
  |     |- [OBJ]_003
  |     |    |- [000]_DASH01_BOARD.004      // Linked Duplicate from [BIND]'s [COLI]_000.
  |     |
  |     |- [OBJ]_004
  |     |    |- [001]_DASH01_SIGN_CIRCLE_A_LOD.001    // Linked Duplicate from [BIND]'s [COLI]_001.
  |     |
  |     |- ...
  |     |- [OBJ]_(n-1)
  |     |- [OBJ]_n
  |
</pre>

TODO: Write TrackSegment's childs in MCTR
