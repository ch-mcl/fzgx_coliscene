import bpy
import importlib

from . import import_coliscene #add-on install
#import import_coliscene #dev
from . import export_coliscene #add-on install
#import export_coliscene #dev

bl_info= {
    "name": "F-ZERO GX/AX COLI_COURSE format",
    "author": "CH-MCL",
    "version": (0, 2),
    "blender": (2, 79, 0),
    "location": "File > Import-Export > F-ZERO GX/AX COLI_COURSE",
    "description": "Imports a F-ZERO GX/AX COLI_COURSE data.",
    "category": "Import-Export",
}

if "bpy" in locals():
    import importlib
    if "import_coliscene" in locals():
        importlib.reload(import_coliscene)
    if "export_coliscene" in locals():
        importlib.reload(export_coliscene)

from bpy.props import (
        BoolProperty,
        #FloatProperty,
        StringProperty,
        #EnumProperty,
        )
from bpy_extras.io_utils import (
#        ImportHelper,
        ExportHelper,
#        orientation_helper_factory,
#        path_reference_mode,
#        axis_conversion,
        )


#Import COLI_COURSE
class IMPORT_UL_ColiScene(bpy.types.Operator):
    bl_idname = 'import_fzgx.colicourse'
    bl_label = 'Import F-ZERO GX/AX COLI_COURSE'
    bl_description = 'Import a F-ZERO GX/AX COLI_COURSE data'
    bl_options = {'REGISTER', 'UNDO'}

    is_ax = BoolProperty(
            name='COLI_COURSE for F-ZERO AX',
            description='COLI_COURSE of F-ZERO AX must Enable this.',
            default=False,
            )

    filepath = StringProperty(
        name='File Path',
        description='Filepath used for importing the F-ZERO GX/AX COLI_COURSE file.',
        maxlen=1024)
    filter_glob = StringProperty(default='COLI_COURSE*,lz', options={'HIDDEN'})

    def execute(self, context):
        keywords = self.as_keywords()
        import_coliscene.load(self.filepath, self.is_ax)
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}


#Export COLI_COURSE
class EXPORT_UL_ColiScene(bpy.types.Operator, ExportHelper):
    bl_idname = 'export_fzgx.colicourse'
    bl_label = '(WIP)Export F-ZERO GX/AX COLI_COURSE'
    bl_description = 'Export a F-ZERO GX/AX COLI_COURSE data'
    bl_options = {'REGISTER', 'UNDO'}

    is_point2point = BoolProperty(
            name='Point-To-Point flag',
            description='if Track is Point-To-Point Enable this.',
            default=False,
            )
    is_ax = BoolProperty(
            name='COLI_COURSE for F-ZERO AX',
            description='COLI_COURSE of F-ZERO AX must Enable this.',
            default=False,
            )

    filename_ext = ',lz'
    filepath = StringProperty(
        name='File Path', 
        description="Filepath used for exporting the F-ZERO GX/AX COLI_COURSE file.", 
        maxlen=1024)
    filter_glob = StringProperty(default='COLI_COURSE*,lz', options={'HIDDEN'})

    def execute(self, context):
        export_coliscene.save(self.filepath, self.is_ax, self.is_point2point)
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}


def menu_func_import(self, context):
    self.layout.operator(IMPORT_UL_ColiScene.bl_idname, text='F-ZERO GX/AX COLI_COURSE')


def menu_func_export(self, context):
   self.layout.operator(EXPORT_UL_ColiScene.bl_idname, text='F-ZERO GX/AX COLI_COURSE')


classes = (
    IMPORT_UL_ColiScene,
    EXPORT_UL_ColiScene
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    
    bpy.types.INFO_MT_file_import.append(menu_func_import)
    bpy.types.INFO_MT_file_export.append(menu_func_export)


def unregister():
    bpy.types.INFO_MT_file_import.remove(menu_func_import)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)

    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
    